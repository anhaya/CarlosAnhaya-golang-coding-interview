FROM golang:alpine

WORKDIR /golang-live-coding-challenge

ADD . .

RUN go mod download

RUN go install -mod=mod github.com/githubnemo/CompileDaemon

ENTRYPOINT CompileDaemon --build="go build" -command="./golang-live-coding-challenge"